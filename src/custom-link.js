import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import View from '@ckeditor/ckeditor5-ui/src/view';
import Swal from 'sweetalert2';
import LinkUI from '@ckeditor/ckeditor5-link/src/linkui';
import toMap from '@ckeditor/ckeditor5-utils/src/tomap';

export default class CustomLink extends Plugin {
    init(){

        const editor = this.editor;
        const config = editor.config.get('customLink');
        if(config && config.enabled){
            const linkUI = editor.plugins.get( LinkUI );
            this.linkFormView = linkUI.formView;
            this.test = linkUI.formview;
            this.button = this._createButton();
    
            this.linkFormView.once('render', () => {
                this.button.render();
                this.linkFormView.registerChild(this.button);
                const list = this.linkFormView.element.children[1];
                this.linkFormView.element.insertBefore(this.button.element, list);
            });
        }
    }

    /*
    _createView(){
        const view = new View();
        const button = this._createButton();
        view.setTemplate( {
            tag: 'ul',
            children: [{
                tag: 'li',
                children: [ button ],
                attributes: {
                    class: [
                        'ck',
                        'ck-list__item'
                    ]
                }
            }],
            attributes: {
                class: [
                    'ck',
                    'ck-reset',
                    'ck-list'
                ]
            }
        } );
        return view;
    }
    */

    _createButton(){
        const editor = this.editor;
        const config = editor.config.get('customLink');
        const button = new ButtonView( this.locale );
        const linkCommand = editor.commands.get('link');

        var label = 'Link File';
        var btnClass = '';
        var btnStyle = '';
        if(config && config.button && config.button.text){
            label = config.button.text;
        }
        if(config && config.button && config.button.class){
            btnClass = config.button.class.join(' ');
        }
        if(config && config.button && config.button.style){
            btnStyle = config.button.style;
        }

        button.set({
            label: label,
            withText: true,
            tooltip: true
        });

        button.extendTemplate( {
			attributes: {
				class: btnClass,
                style: btnStyle
			}
		} );

        button.bind('isEnabled').to(linkCommand);

        button.on('execute', () => {
            this._openModal();
        })

        return button;
    }

    _openModal(){
        const editor = this.editor;
        editor.editing.view.focus();
        const config = editor.config.get('imageEditor');
        const config2 = editor.config.get('customLink');
        var files = []
        var fileList = []
        var page = 1;
        var pagesize = 5;
        var selectSrc = '';
        var selectText = '';
        if(config && config.table && config.table.rows){
            pagesize = config.table.rows;
        }
        var maxpage = 1;
        var modal = '';

        var checkFileType = (filename) => {
            var temp = filename.split('.');
            if(config.imageTypes.includes(temp[temp.length - 1].toLowerCase())){
                return 'image';
            }
            else{
                return 'text';
            }
        }

        Swal.fire({
            title: config ? (config2.modalTitle ? config2.modalTitle : 'Insert Link') : 'Insert Link',
            width: config ? (config.modalWidth ? config.modalWidth : 800) : 800,
            showCancelButton: true,
            showConfirmButton: false,
            buttonsStyling: config ? ( config.cancelButtonClass ? false : true ) : true,
            customClass: {
                cancelButton: config ? ( config.cancelButtonClass ? config.cancelButtonClass.join(' ') : '') : '',
            },
            heightAuto: false,
            confirmButtonText: 'Insert',
            html: '<div>'+
                '<input type="file" multiple id="fileupload" style="position: fixed; top: -100em;">'+
                '<hr>'+
                '<div>'+
                '<input id="search-input" type="text">'+
                '<span><button id="upload-button">Upload</button></span>'+
                `<div id="upload-container">`+
                `</div>`+
                '</div>'+
                '<div style="margin-top: 25px; margin-bottom: 25px">'+
                '<table id="image-editor-table" style="width: 100%">' +
                '</table></div>' +
                '<div id="button-div" style="margin-top: 25px; margin-bottom: 25px;">'+
                '<hr>' +
                '<button id="firstpage-button"></button>'+
                '<button id="prevpage-button"></button>'+
                '<span id="page-buttons-span"></span>'+
                '<button id="nextpage-button"></button>'+
                '<button id="lastpage-button"></button>'+
                '</div>'+
                '<hr>' +
                '</div>',
            preConfirm: function() {
                if(selectSrc != '' && selectText != ''){
                    return {pass: true, src: selectSrc, text: selectText}
                }
                return {pass: false};
            },
            onAfterClose: () => {
                editor.editing.view.focus();
            },
            onOpen: function (e) {
                modal = e;
                var createHtml = () => {
                    createTable();
                    searchEnabled();
                    setClass();
                    setStyle();
                    config.files()
                    .then(results => {
                        files = results;
                        fileList = results;
                        maxpage = Math.max(Math.ceil(files.length / pagesize), 1);
                        tableBodyFunction();
                        checkPage();
                        if (maxpage <= 1) {
                            e.getElementsByTagName('div').namedItem('button-div').style.display = 'none';
                        }
                        if (config && config.upload) {
                            if (config.upload.enabled == false || config.upload.enabled == undefined || config.upload.enabled == null) {
                                e.getElementsByTagName('button').namedItem('upload-button').remove();
                            }
                            else {
                                e.getElementsByTagName('button').namedItem('upload-button').addEventListener('click', function () {
                                    e.getElementsByTagName('input').namedItem('fileupload').click();
                                });
                                e.getElementsByTagName('input').namedItem('fileupload').addEventListener('change', function ($event) {
                                    upload($event);
                                    tableBodyFunction();
                                    checkPage();
                                });
                            }
                        }
                        else {
                            e.getElementsByTagName('button').namedItem('upload-button').remove();
                        }
                    })
                }

                var upload = (event) => {
                    var temp = event.target.files;
                    var uploads = [];
                    for(var i = 0; i < temp.length; i++){
                        uploads.push(temp[i]);
                    }
                    uploads.map(file => {
                        config.upload.function(file)
                        .then(result => {
                            var xhr = new XMLHttpRequest();
                            xhr.open(result.method, result.uploadUrl, true);
                            if(result.headers){
                                Object.keys(result.headers).map(key => {
                                    xhr.setRequestHeader(key, result.headers[key]);
                                });
                            }
                            var div = document.createElement('div');
                            var failure = () => {
                                if(config.upload && config.upload.container && config.upload.container.failure && config.upload.container.failure.color){
                                    progressBar.style.backgroundColor = config.upload.container.failure.color;
                                    span.style.color = config.upload.container.failure.color;
                                }
                                else{
                                    progressBar.style.backgroundColor = 'red';
                                    span.style.color = 'red';
                                }
                                if(config.upload && config.upload.container && config.upload.container.failure && config.upload.container.failure.duration){
                                    setTimeout(() => {
                                        div.remove();
                                    }, config.upload.container.failure.duration)
                                }
                                else{
                                    setTimeout(() => {
                                        div.remove();
                                    }, 5000)
                                }
                            }
                            if(config.upload && config.upload.container && config.upload.container.class){
                                div.classList = config.upload.container.class.join(' ');
                            }
                            if(config.upload && config.upload.container && config.upload.container.style){
                                Object.keys(config.upload.container.style).map(key => {
                                    div.style[key] = config.upload.container.style[key];
                                });
                            }
                            var span = document.createElement('span');
                            span.innerText = file.name;
                            var progressMeter = document.createElement('div');
                            if(config.upload && config.upload.container && config.upload.container.progressMeter && config.upload.container.progressMeter.class){
                                progressMeter.classList = config.upload.container.progressMeter.class.join(' ');
                            }
                            if(config.upload && config.upload.container && config.upload.container.progressMeter && config.upload.container.progressMeter.style){
                                Object.keys(config.upload.container.progressMeter.style).map(key => {
                                    progressMeter.style[key] = config.upload.container.progressMeter.style[key];
                                });
                            }
                            else{
                                progressMeter.style.width = "100%";
                                progressMeter.style.backgroundColor = '#ddd';
                            }
                            var progressBar = document.createElement('div');
                            if(config.upload && config.upload.container && config.upload.container.progressBar && config.upload.container.progressBar.class){
                                progressBar.classList = config.upload.container.progressBar.class.join(' ');
                            }
                            if(config.upload && config.upload.container && config.upload.container.progressBar && config.upload.container.progressBar.style){
                                Object.keys(config.upload.container.progressBar.style).map(key => {
                                    progressBar.style[key] = config.upload.container.progressBar.style[key];
                                });
                                progressBar.style.width = "0%";
                            }
                            else{
                                progressBar.style.width = "0%";
                                progressBar.style.height = "30px";
                                progressBar.style.backgroundColor = "#4CAF50";
                            }
                            var container = e.getElementsByTagName('div').namedItem('upload-container');
                            div.prepend(span);
                            progressMeter.append(progressBar);
                            div.append(progressMeter);
                            container.append(div);
                            xhr.upload.onprogress = (e) => {
                                if(e.lengthComputable){
                                    progressBar.style.width = Math.round((e.loaded * 100) / e.total) + "%";
                                }
                            }
                            xhr.upload.addEventListener('error', (evt) => {
                                console.log(evt || 'Error');
                                failure();
                            });
                            xhr.upload.addEventListener('abort', (evt) => {
                                console.log(evt || 'Abort');
                                failure();
                            });
                            xhr.upload.addEventListener('timeout', (evt) => {
                                console.log(evt || 'Timeout');
                                failure();
                            });
                            xhr.onreadystatechange = () => {
                                if(xhr.readyState === XMLHttpRequest.DONE){
                                    if(xhr.status == 201){
                                        config.files()
                                        .then(results => {
                                            div.remove();
                                            files = results;
                                            fileList = results;
                                            maxpage = Math.max(Math.ceil(files.length / pagesize), 1);
                                            tableBodyFunction();
                                            checkPage();
                                            e.getElementsByTagName('input').namedItem('search-input').value = '';
                                        });
                                    }
                                    else{
                                        console.log('Failure');
                                        failure();
                                    }
                                }
                            }
                            xhr.send(result.file);
                        })
                        .catch(err => {
                            console.log(err);
                        });
                    });
                }

                var checkPage = () => {
                    if(page == 1 && e.getElementsByTagName('div').namedItem('button-div')){
                        e.getElementsByTagName('button').namedItem('prevpage-button').disabled = true;
                        e.getElementsByTagName('button').namedItem('firstpage-button').disabled = true;
                    }
                    if(page < maxpage && e.getElementsByTagName('div').namedItem('button-div')){
                        e.getElementsByTagName('button').namedItem('nextpage-button').disabled = false;
                        e.getElementsByTagName('button').namedItem('lastpage-button').disabled = false;
                    }
                    if(page > 1 && e.getElementsByTagName('div').namedItem('button-div')){
                        e.getElementsByTagName('button').namedItem('prevpage-button').disabled = false;
                        e.getElementsByTagName('button').namedItem('firstpage-button').disabled = false;
                    }
                    if(page == maxpage && e.getElementsByTagName('div').namedItem('button-div')){
                        e.getElementsByTagName('button').namedItem('nextpage-button').disabled = true;
                        e.getElementsByTagName('button').namedItem('lastpage-button').disabled = true;
                    }
                }

                var setupPageButtons = () => {
                    var div = e.getElementsByTagName('span').namedItem('page-buttons-span');
                    e.getElementsByTagName('span').namedItem('page-buttons-span').innerHTML = '';
                    for(var i = -2; i <= 2; i++){
                        if((page + i) >= 1 && (page + i) <= maxpage){
                            var btn = document.createElement('button');
                            btn.value = page + i;
                            btn.innerText = page + i;
                            if(page + i == page){
                                var classList = [];
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.class){
                                    config.pageBtns.numberPage.class.map(o => {
                                        classList.push(o);
                                    });
                                }
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.active && config.pageBtns.numberPage.active.class){
                                    config.pageBtns.numberPage.active.class.map(o => {
                                        classList.push(o);
                                    })
                                }
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.style){
                                    Object.keys(config.pageBtns.numberPage.style).map(key => {
                                        btn.style[key] = config.pageBtns.numberPage.style[key];
                                    });
                                }
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.active && config.pageBtns.numberPage.active.style){
                                    Object.keys(config.pageBtns.numberPage.active.style).map(key => {
                                        btn.style[key] = config.pageBtns.numberPage.active.style[key];
                                    });
                                }
                                btn.classList = classList.join(' ');
                            }
                            else{
                                var classList = [];
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.class){
                                    btn.classList = config.pageBtns.numberPage.class.join(' ');
                                }
                                if(config && config.pageBtns && config.pageBtns.numberPage && config.pageBtns.numberPage.style){
                                    Object.keys(config.pageBtns.numberPage.style).map(key => {
                                        btn.style[key] = config.pageBtns.numberPage.style[key];
                                    });
                                }
                            }
                            btn.addEventListener('click', function(){
                                page = Number(this.value);
                                tableBodyFunction();
                                checkPage();
                            });
                            e.getElementsByTagName('span').namedItem('page-buttons-span').appendChild(btn);
                        }
                    }
                    if(config && config.pageBtns && config.pageBtns.prevPage && config.pageBtns.prevPage.text){
                        e.getElementsByTagName('button').namedItem('prevpage-button').innerText = config.pageBtns.prevPage.text;
                    }
                    else{
                        e.getElementsByTagName('button').namedItem('prevpage-button').innerText = '<';
                    }
                    if(config && config.pageBtns && config.pageBtns.firstPage && config.pageBtns.firstPage.text){
                        e.getElementsByTagName('button').namedItem('firstpage-button').innerText = config.pageBtns.firstPage.text;
                    }
                    else{
                        e.getElementsByTagName('button').namedItem('firstpage-button').innerText = '<<';
                    }
                    if(config && config.pageBtns && config.pageBtns.nextPage && config.pageBtns.nextPage.text){
                        e.getElementsByTagName('button').namedItem('nextpage-button').innerText = config.pageBtns.nextPage.text;
                    }
                    else{
                        e.getElementsByTagName('button').namedItem('nextpage-button').innerText = '>';
                    }
                    if(config && config.pageBtns && config.pageBtns.lastPage && config.pageBtns.lastPage.text){
                        e.getElementsByTagName('button').namedItem('lastpage-button').innerText = config.pageBtns.lastPage.text;
                    }
                    else{
                        e.getElementsByTagName('button').namedItem('lastpage-button').innerText = '>>';
                    }
                    if(maxpage == 1){
                        e.getElementsByTagName('div').namedItem('button-div').style.display = 'none';
                    }
                    else{
                        e.getElementsByTagName('div').namedItem('button-div').style.display = 'block';
                    }
                }

                var searchEnabled = () => {
                    if(config.search && config.search.enabled){
                        e.getElementsByTagName('input').namedItem('search-input').addEventListener('input', function() {
                            files = [];
                            Promise.all([
                                fileList.map(o => {
                                    if(o[config.search.searchField].toLowerCase().includes(this.value)){
                                        files.push(o);
                                    }
                                })
                            ])
                            .then(results => {
                                maxpage = Math.max(Math.ceil(files.length / pagesize), 1);
                                page = 1;
                                if(files.length == 0){
                                    emptyTableFunction();
                                }
                                else{
                                    tableBodyFunction();
                                }
                                checkPage();
                            })
                        })
                    }
                    else{
                        e.getElementsByTagName('input').namedItem('search-input').remove();
                    }
                }

                var setClass = () => {
                    if(config.table && config.table.class){
                        e.getElementsByTagName('table')[0].classList = config.table.class.join(' ');
                    }
                    if(config.search && config.search.class && e.getElementsByTagName('input').namedItem('search-input')){
                        e.getElementsByTagName('input').namedItem('search-input').classList = config.search.class.join(' ');
                    }
                    if(config.pageBtns && config.pageBtns.nextPage && config.pageBtns.nextPage.class){
                        e.getElementsByTagName('button').namedItem('nextpage-button').classList = config.pageBtns.nextPage.class.join(' ');
                    }
                    if(config.pageBtns && config.pageBtns.prevPage && config.pageBtns.prevPage.class){
                        e.getElementsByTagName('button').namedItem('prevpage-button').classList = config.pageBtns.prevPage.class.join(' ');
                    }
                    if(config.pageBtns && config.pageBtns.firstPage && config.pageBtns.firstPage.class){
                        e.getElementsByTagName('button').namedItem('firstpage-button').classList = config.pageBtns.firstPage.class.join(' ');
                    }
                    if(config.pageBtns && config.pageBtns.lastPage && config.pageBtns.lastPage.class){
                        e.getElementsByTagName('button').namedItem('lastpage-button').classList = config.pageBtns.lastPage.class.join(' ');
                    }
                    if(config.upload && config.upload.class){
                        e.getElementsByTagName('button').namedItem('upload-button').classList = config.upload.class.join(' ');
                    }
                }

                var setStyle = () => {
                    if(config.table && config.table.style){
                        Object.keys(config.table.style).map(key => {
                            e.getElementsByTagName('table')[0].style[key] = config.table.style[key];
                        });
                    }
                    if(config.search && config.search.style && e.getElementsByTagName('input').namedItem('search-input')){
                        Object.keys(config.search.style).map(key => {
                            e.getElementsByTagName('input').namedItem('search-input').style[key] = config.search.style[key];
                        });
                    }
                    if(config.pageBtns && config.pageBtns.nextPage && config.pageBtns.nextPage.style){
                        Object.keys(config.pageBtns.nextPage.style).map(key => {
                            e.getElementsByTagName('button').namedItem('nextpage-button').style[key] = config.pageBtns.nextPage.style[key];
                        });
                    }
                    if(config.pageBtns && config.pageBtns.prevPage && config.pageBtns.prevPage.style){
                        Object.keys(config.pageBtns.prevPage.style).map(key => {
                            e.getElementsByTagName('button').namedItem('prevpage-button').style[key] = config.pageBtns.prevPage.style[key];
                        });
                    }
                    if(config.pageBtns && config.pageBtns.firstPage && config.pageBtns.firstPage.style){
                        Object.keys(config.pageBtns.firstPage.style).map(key => {
                            e.getElementsByTagName('button').namedItem('firstpage-button').style[key] = config.pageBtns.firstPage.style[key];
                        });
                    }
                    if(config.pageBtns && config.pageBtns.lastPage && config.pageBtns.lastPage.style){
                        Object.keys(config.pageBtns.lastPage.style).map(key => {
                            e.getElementsByTagName('button').namedItem('lastpage-button').style[key] = config.pageBtns.lastPage.style[key];
                        });
                    }
                    if(config.upload && config.upload.style){
                        Object.keys(config.upload.style).map(key => {
                            e.getElementsByTagName('button').namedItem('upload-button').style[key] = config.upload.style[key];
                        });
                    }
                }

                var createTable = () => {
                    var thead = document.createElement('thead');
                    var tr = document.createElement('tr');
                    config.table.columns.map(o => {
                        var th = document.createElement('th');
                        th.innerText = o.header;
                        if(o.th && o.th.class){
                            th.classList = o.th.class.join(' ');
                        }
                        if(o.th && o.th.style){
                            Object.keys(o.th.style).map(key => {
                                th.style[key] = o.th.style[key];
                            })
                        }
                        tr.append(th);
                    })
                    if(config.table && config.table.preview && config.table.preview.enabled){
                        var th2 = document.createElement('th');
                        th2.innerText = 'Preview';
                        if(config.table && config.table.preview && config.table.preview.th && config.table.preview.th.text){
                            th2.innerText = config.table.preview.th.text;
                        }
                        if(config.table && config.table.preview && config.table.preview.th && config.table.preview.th.class){
                            th2.classList = config.table.preview.th.class.join(' ');
                        }
                        if(config.table && config.table.preview && config.table.preview.th && config.table.preview.th.style){
                            Object.keys(config.table.preview.th.style).map(key => {
                                th2.style[key] = config.table.preview.th.style[key];
                            })
                        }
                        tr.prepend(th2);
                    }
                    var th3 = document.createElement('th');
                    th3.innerText = '';
                    if(config.table && config.table.btn && config.table.btn.th && config.table.btn.th.text){
                        th3.innerText = config.table.btn.th.text;
                    }
                    if(config.table && config.table.btn && config.table.btn.th && config.table.btn.th.class){
                        th3.classList = config.table.btn.th.class.join(' ');
                    }
                    if(config.table && config.table.btn && config.table.btn.th && config.table.btn.th.style){
                        Object.keys(config.table.btn.th.style).map(key => {
                            th3.style[key] = config.table.btn.th.style[key];
                        });
                    }
                    tr.append(th3);
                    thead.append(tr);
                    e.getElementsByTagName('table')[0].appendChild(thead);
                    tableBodyFunction();
                }

                var tableBodyFunction = () => {
                    var tbody = document.createElement('tbody');
                    for(var i = ((page - 1) * pagesize); i < Math.min(page * pagesize, files.length); i++){
                        var file = files[i];
                        var tr = document.createElement('tr');
                        config.table.columns.map(o => {
                            var td = document.createElement('td');
                            if(o.link && o.link.enabled){
                                var a = document.createElement("a");
                                a.href = file[o.link.src];
                                a.text = file[o.variable];
                                a.target = "_blank";
                                td.append(a);
                            }
                            else{
                                td.innerText = file[o.variable];
                            }
                            if(o.td && o.td.class){
                                td.classList = o.td.class.join(' ');
                            }
                            if(o.td && o.td.style){
                                Object.keys(o.td.style).map(key => {
                                    td.style[key] = o.td.style[key];
                                });
                            }
                            tr.append(td);
                        });
                        if(config.table && config.table.preview && config.table.preview.enabled){
                            let td2 = document.createElement('td');
                            if(config.table.preview.td && config.table.preview.td.class){
                                td2.classList = config.table.preview.td.class.join(' ');
                            }
                            if(config.table.preview.td && config.table.preview.td.style){
                                Object.keys(config.table.preview.td.style).map(key => {
                                    td2.style[key] = config.table.preview.td.style[key];
                                });
                            }
                            if(config.table.preview.src){
                                if(file[config.table.preview.src] && checkFileType(file[config.table.preview.src]) == 'image'){
                                    var img = document.createElement('img');
                                    img.src = file[config.table.preview.src];
                                    if(config.table.preview.class){
                                        img.classList = config.table.preview.class.join(' ');
                                    }
                                    if(config.table.preview.style){
                                        Object.keys(config.table.preview.style).map(key => {
                                            img.style[key] = config.table.preview.style[key];
                                        });
                                    }
                                    td2.appendChild(img);
                                }
                                else{
                                    td2.innerText = 'N/A';
                                }
                            }
                            tr.prepend(td2);
                        }
                        let td3 = document.createElement('td');
                        if(config.table.btn && config.table.btn.td && config.table.btn.td.class){
                            td3.classList = config.table.btn.td.class.join(' ');
                        }
                        if(config.table.btn && config.table.btn.td && config.table.btn.td.style){
                            Object.keys(config.table.btn.td.style).map(key => {
                                td3.style[key] = config.table.btn.td.style[key];
                            })
                        }
                        var btn = document.createElement('button');
                        if(config && config.table && config.table.btn && config.table.btn.class){
                            btn.classList = config.table.btn.class.join(' ');
                        }
                        btn.value = file[config.table.btn.src] + '||' + (file[config2.button.filename] || file[config.table.btn.src]);
                        btn.innerText = 'Select';
                        if(config.table.btn && config.table.btn.style){
                            Object.keys(config.table.btn.style).map(key => {
                                btn.style[key] = config.table.btn.style[key];
                            });
                        }
                        btn.addEventListener('click', function() {
                            var temp = this.value.split('||');
                            selectSrc = temp[0];
                            selectText = temp[1] || temp[0];
                            Swal.clickConfirm();
                        })
                        td3.appendChild(btn);
                        tr.append(td3);
                        tbody.append(tr);
                    }
                    if(e.getElementsByTagName('tbody')[0]){
                        e.getElementsByTagName('tbody')[0].remove();
                    }
                    e.getElementsByTagName('table')[0].appendChild(tbody);
                    setupPageButtons();
                }

                var emptyTableFunction = () => {
                    var tbody = document.createElement('tbody');
                    var tr = document.createElement('tr');
                    var td = document.createElement('td');
                    td.innerText = "No Files Found";
                    td.colSpan = config.table.columns.length + 1;
                    if(config.table && config.table.preview && config.table.preview.enabled){
                        td.colSpan++;
                    }
                    tr.append(td);
                    tbody.append(tr);
                    if(e.getElementsByTagName('tbody')[0]){
                        e.getElementsByTagName('tbody')[0].remove();
                    }
                    e.getElementsByTagName('table')[0].appendChild(tbody);
                }

                e.getElementsByTagName('button').namedItem("nextpage-button").addEventListener('click', function() {
                    page++;
                    tableBodyFunction();
                    checkPage();
                });

                e.getElementsByTagName('button').namedItem('prevpage-button').addEventListener('click', function() {
                    page--;
                    tableBodyFunction();
                    checkPage();
                });

                e.getElementsByTagName('button').namedItem('firstpage-button').addEventListener('click', function() {
                    page = 1;
                    tableBodyFunction();
                    checkPage();
                });

                e.getElementsByTagName('button').namedItem('lastpage-button').addEventListener('click', function() {
                    page = maxpage;
                    tableBodyFunction();
                    checkPage();
                });

                createHtml();
            }
        })
        .then(result => {
            if(result.value && result.value.pass){
                const model = this.editor.model;
                const selection = model.document.selection;
                editor.model.change(writer => {
                    if ( selection.isCollapsed ) {
                        const position = selection.getFirstPosition();
                        if ( selection.hasAttribute( 'linkHref' ) ) {
                            editor.execute('link', result.value.src, {linkIsExternal: true});
                        }
                        else {
                            const attributes = toMap( selection.getAttributes() );     
                            const range = model.insertContent( writer.createText( result.value.text, attributes ), position );
                            writer.setSelection(range);
                            editor.execute('link', result.value.src, {linkIsExternal: true});
                        }
                    }
                    else{
                        editor.execute('link', result.value.src, {linkIsExternal: true});
                    }
                });
            }
        });
    }
}