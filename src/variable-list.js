import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import Swal from 'sweetalert2';
import imageIcon from './var.svg';

export default class VariableList extends Plugin {
    init(){
        const editor = this.editor;
        var config = editor.config.get('variableList');

        editor.ui.componentFactory.add('variableList', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: config.label,
                icon:  imageIcon,
                tooltip: true
            });

            view.on('execute', () => {
                editor.editing.view.focus();
                config = editor.config.get('variableList');
                Swal.fire({
                    title: config.label,
                    width: config ? (config.modalWidth ? config.modalWidth : 500) : 500,
                    showCancelButton: true,
                    showConfirmButton: false,
                    buttonsStyling: config ? ( config.cancelButtonClass ? false : true ) : true,
                    customClass: {
                        cancelButton: config ? ( config.cancelButtonClass ? config.cancelButtonClass.join(' ') : '') : '',
                    },
                    html: '<hr><div id="list-container" style="width: 100%"; margin-bottom: 25px></div>'+
                    '<div id="variable-container" style="width: 100%;"></div><hr style="clear: both;">',
                    onAfterClose: () => {
                        editor.editing.view.focus();
                    },
                    onOpen: function(e){
                        var div = e.getElementsByTagName('div').namedItem('variable-container')
                        if(config && config.multipleLists && config.lists && config.lists.length > 0){
                            config.variables.map(o => {
                                if(o.listId == config.lists[0].id){
                                    var btn = document.createElement('button');
                                    btn.value = o.insert;
                                    btn.innerHTML = o.label;
                                    btn.style.display = 'block';
                                    btn.style.width = '100%';
                                    btn.style.marginBottom = '5px';
                                    btn.classList = config.varBtn ? (config.varBtn .class ? config.varBtn.class.join(' ') : '') : '';
                                    if(config.varBtn  && config.varBtn.style){
                                        Object.keys(config.varBtn.style).map(key => {
                                            btn.style[key] = config.varBtn.style[key];
                                        });
                                    }
                                    btn.addEventListener('click', function(){
                                        editor.model.change(writer => {
                                            writer.remove(editor.model.document.selection.getFirstRange());
                                            writer.insertText(this.value, editor.model.document.selection.getFirstPosition());
                                        });
                                        Swal.close();
                                    });
                                    div.appendChild(btn);
                                }
                            });
                            config.lists.map(list => {
                                var btn = document.createElement('button');
                                btn.value = list.id;
                                btn.innerHTML = list.label;
                                btn.classList = config.listBtn ? (config.listBtn.class ?  config.listBtn.class.join(' ') : '') : '';
                                if(config.listBtn && config.listBtn.style){
                                    Object.keys(config.listBtn.style).map(key => {
                                        btn.style[key] = config.listBtn.style[key];
                                    });
                                }
                                btn.addEventListener('click', function(){
                                    div.innerHTML = '';
                                    config.variables.map(o => {
                                        if(o.listId == this.value){
                                            var btn = document.createElement('button');
                                            btn.value = o.insert;
                                            btn.innerHTML = o.label;
                                            btn.style.display = 'block';
                                            btn.style.width = '100%';
                                            btn.style.marginBottom = '5px';
                                            btn.classList = config.varBtn ? (config.varBtn.class ? config.varBtn.class.join(' ') : '') : '';
                                            if(config.varBtn && config.varBtn.style){
                                                Object.keys(config.varBtn.style).map(key => {
                                                    btn.style[key] = config.varBtn.style[key];
                                                });
                                            }
                                            btn.addEventListener('click', function(){
                                                editor.model.change(writer => {
                                                    writer.remove(editor.model.document.selection.getFirstRange());
                                                    writer.insertText(this.value, editor.model.document.selection.getFirstPosition());
                                                });
                                                Swal.close();
                                            });
                                            div.appendChild(btn);
                                        }
                                    });
                                });
                                e.getElementsByTagName('div').namedItem('list-container').appendChild(btn);
                            });
                            var HR = document.createElement('HR');
                            HR.style.clear = 'both';
                            e.getElementsByTagName('div').namedItem('list-container').appendChild(HR);
                        }
                        else{
                            e.getElementsByTagName('div').namedItem('list-container').remove();
                            config.variables.map(o => {
                                var btn = document.createElement('button');
                                btn.value = o.insert;
                                btn.innerHTML = o.label;
                                btn.style.display = 'block';
                                btn.style.width = '100%';
                                btn.style.marginBottom = '5px';
                                btn.classList = config.varBtn ? (config.varBtn.class ? config.varBtn.class.join(' ') : '') : '';
                                if(config.varBtn && config.varBtn.style){
                                    Object.keys(config.varBtn.style).map(key => {
                                        btn.style[key] = config.varBtn.style[key];
                                    });
                                }
                                btn.addEventListener('click', function(){
                                    editor.model.change(writer => {
                                        writer.remove(editor.model.document.selection.getFirstRange());
                                        writer.insertText(this.value, editor.model.document.selection.getFirstPosition());
                                    });
                                    Swal.close();
                                });
                                div.appendChild(btn);
                            })
                        }
                    }
                })
            });
            return view;
        });
    }
}