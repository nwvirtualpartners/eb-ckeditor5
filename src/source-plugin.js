import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview';
import Swal from 'sweetalert2';
import imageIcon from './source.svg';

export default class SourcePlugin extends Plugin {
    init(){
        var editor = this.editor;

        editor.ui.componentFactory.add('sourcePlugin', locale => {
            const view = new ButtonView(locale);

            view.set({
                label: 'View Source',
                icon:  imageIcon,
                tooltip: true
            });

            view.on('execute', () => {
                var config = editor.config.get('sourcePlugin');
                if(config && config.custom && config.customFunction){
                    config.customFunction();
                }
                else{
                    editor.editing.view.focus();
                    var textarea = null;
                    Swal.fire({
                        title: 'View Source',
                        width: 1000,
                        showCancelButton: true,
                        showConfirmButton: true,
                        buttonsStyling: config ? ( config.cancelButtonClass || config.confirmButtonClass ? false : true ) : true,
                        showClass: {
                            popup: 'swal2-noanimation',
                            backdrop: 'swal2-noanimation'
                        },
                        customClass: {
                            cancelButton: config ? ( config.cancelButtonClass ? config.cancelButtonClass.join(' ') : '') : '',
                            confirmButton: config ? ( config.confirmButtonClass ? config.confirmButtonClass.join(' ') : '') : ''
                        },
                        html: '<hr>'+
                        '<textarea id="source-container" style="width: 100%; min-height: 400px;"></textarea><hr style="clear: both;">',
                        preConfirm: function() {
                            return { value: true, text: textarea.value }
                        },
                        onAfterClose: () => {
                            editor.editing.view.focus();
                        },
                        onOpen: function(e){
                            var format = (node, level) => {
                                var indentBefore = new Array(level++ + 1).join('  '),
                                indentAfter  = new Array(level - 1).join('  '),
                                textNode;
                            
                                for (var i = 0; i < node.children.length; i++) {
                                    
                                    textNode = document.createTextNode('\n' + indentBefore);
                                    node.insertBefore(textNode, node.children[i]);
                                    
                                    format(node.children[i], level);
                                    
                                    if (node.lastElementChild == node.children[i]) {
                                        textNode = document.createTextNode('\n' + indentAfter);
                                        node.appendChild(textNode);
                                    }
                                }
                                
                                return node;
                            }
                            textarea = e.getElementsByTagName('textarea').namedItem('source-container')
                            var source = editor.getData();
                            var div = document.createElement('div');
                            div.innerHTML = source.trim();
                            textarea.value = format(div, 0).innerHTML.trim();
                        }
                    })
                    .then(results => {
                        if(results.value){
                            editor.setData(results.value.text);
                        }
                    });
                }
            });
            return view;
        });
    }
}