import Plugin from '@ckeditor/ckeditor5-core/src/plugin'
import LinkUI from '@ckeditor/ckeditor5-link/src/linkui';

export default class LinkProtocol extends Plugin {
    init() {
        const editor = this.editor;
        const linkUI = editor.plugins.get(LinkUI);
        const config = editor.config.get('linkProtocol');

        this.linkFormView = linkUI.formView;

        this.linkFormView.on('submit', (e) => {
            return;
            if(config && config.enable){
                if(config.protocols){
                    console.log(this.linkFormView);
                    var href = this.linkFormView.urlInputView.fieldView.value.toLowerCase();
                    var test = config.protocols.filter(o => href.includes(o));
                    if(test.length == 0){
                        var temp = this.editor.getData();
                        if(config.insertProtocol){
                            temp = temp.replace('href="' + this.linkFormView.urlInputView.fieldView.value + '"', 'href="' + config.insertProtocol + this.linkFormView.urlInputView.fieldView.value + '"');
                        }
                        else{
                            temp = temp.replace('href="' + this.linkFormView.urlInputView.fieldView.value + '"', 'href="https://' + this.linkFormView.urlInputView.fieldView.value + '"');
                        }
                        this.editor.setData(temp);
                    }
                }
                else{
                    var href = this.linkFormView.urlInputView.fieldView.value.toLowerCase();
                    var protocols = ['https://', 'http://', 'mailto:', 'file://', 'ftp://'];
                    var test = protocols.filter(o => href.includes(o));
                    if(test.length == 0){
                        var temp = this.editor.getData();
                        if(config.insertProtocol){
                            temp = temp.replace('href="' + this.linkFormView.urlInputView.fieldView.value + '"', 'href="' + config.insertProtocol + this.linkFormView.urlInputView.fieldView.value + '"');
                        }
                        else{
                            temp = temp.replace('href="' + this.linkFormView.urlInputView.fieldView.value + '"', 'href="https://' + this.linkFormView.urlInputView.fieldView.value + '"');
                        }
                        this.editor.setData(temp);
                    }
                }
            }
        });
    }
}